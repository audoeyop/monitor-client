exports.commands = [
  {
	operation: "get.system.config",
	command: "sysctl -a",
  },
  {
  	operation: "get.processes",
  	command: "top -b -n1",
  },
  {
  	operation: "get.network.interfaces",
  	command: "ifconfig",
  },
  {
    operation: "get.hostname",
    command: "hostname",
  },
  // {
  // 	operation: "get.system.log",
  // 	command: "grep \"sshd.*\\(Failed\\|Accepted\\|Postponed\\|PAM:\\sauthentication\\serror\\)\" /var/log/system.log",
  // },
  {
  	operation: "get.process.details",
  	command: "ps -al",
  },
  {
  	operation: "get.disk.status",
  	command: "df -h",
  },

  // {
  // 	operation: "get.users",
  // 	command: "dscl . -readall /Users",
  // },
  {
    operation: "get.users.active",
    command: "users",
  },
  {
    operation: "get.files.open",
    command: "lsof",
    interval: 900000,
  },
];
