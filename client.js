var io = require('socket.io-client');
var fs = require('fs');
var shell = require('shelljs');
var WebSocketClient = require('websocket').client;
var local_config = require('./config/local-config');
var YAML = require('yamljs');
var os = require("os");

var hostname = os.hostname();
var interval = 60000;
var command_file_prefix = "command-";
var command_file_postfix = "";
var remove_non_aplhanumeric_regex = /[^a-zA-Z0-9 :]/g;
var replace_non_alphanumeric_with = "";
var config = [];
var operating_system = shell_command('bash', 'uname -a');

var commands = servers = [];
var sockets = {};
var token = "";

process.argv.forEach(function(arg, index) {

  switch(arg) {

    case '-t':

        if(process.argv[index+1]) {

          token = process.argv[index+1];
        };

        break;

    case '-s':

        if(process.argv[index+1]) {

          servers.push(process.argv[index+1]);
        };

        break;
    };
});

var error = false;

if(!token) {

  console.log('error: no token [-t <token>]');

  error = true;
}

if(!servers.length) {

  console.log('error: no server [-s <server>]');

  error = true;
};

if(error) {

  process.exit();
}

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function in_config(config, setting, config_value) {

  var output = false;
  for(index in config){
    var value = config[index];
    if(value && value[0]) {
      var regex = value[0].match(/(^.*)(=)(.*)/);
      var attr;
      var val;

      // config attribute
      if(regex && regex[1]){
        attr = regex[1];
      }

      // config value
      if(regex && regex[3]){
        val = regex[3];
      }

      if(attr && val){
        if(isNumeric(val)) {
          val = parseFloat(val);
        }
      }

      if(attr == setting && val == config_value) {
        return true;
      }
    }

    if(index >= (config.length - 1)) {
      return false;
    }
  }

  return false;
}

function emit_information(socket) {
  commands.forEach(function(value, index) {
    var emit_value = interval;
    if(value["interval"]) {
      emit_value = parseInt(value["interval"]);
    }

    if(in_config(config, value.operation, 1)){
      operation = value.operation;
      emit_interval(socket, emit_value, value.command, operation);
    }
  });
}


function get_config(processor, socket) {

  var message_label = "get configuration";

  var message_content = {hostname: hostname, token: token};

  emit (socket, message(processor, message_label, message_content));

  console.log("waiting for response from server " + processor.server);
}

function message(processor, label, content) {

  return {processor: processor, content: content, label: label};
}


function shell_command (os_shell, command) {

  if (os_shell === "bash") {

    return shell.exec(command, {silent:true}).stdout
  }
}

function emit (socket, message) {

  console.log("emit: " + message.processor.server + " \t-> '" + message.label + "': '" + message.content + "'");

  socket.emit(message.label, message.content);
};

function recieve (socket, message) {

  console.log("recieve: " + message.processor.token + " \t<- '" + message.label + "': '" + message.content + "'");

  return;
}

// function emit_interval (socket, interval, command, operation) {
//   fs.exists(command_file_prefix + command.replace(remove_non_aplhanumeric_regex, replace_non_alphanumeric_with) + command_file_postfix, function(exists) {
//     if (exists) {
//       fs.unlinkSync(command_file_prefix + command.replace(remove_non_aplhanumeric_regex, replace_non_alphanumeric_with) + command_file_postfix);
//     }
//   });
//
//   setup_output (socket, command, operation);
//
//   setInterval(function(){
//     setup_output (socket, command, operation);
//   }, interval);
// }

function setup_output (socket, command, operation) {

    var output = {};
    output["operating_system"] = operating_system;
    output["operation"] = operation;
    output["command"] = command;
    output["interval"] = interval;
    output[command] = shell_command('bash', command);

    file_the_same(socket, command, output);
}

function file_the_same (socket, command, output) {

  fs.exists(command_file_prefix + command.replace(remove_non_aplhanumeric_regex, replace_non_alphanumeric_with) + command_file_postfix, function(exists) {
    if (exists) {
      fs.readFile(command_file_prefix + command.replace(remove_non_aplhanumeric_regex, replace_non_alphanumeric_with) + command_file_postfix, 'utf8', function (err,file_content) {

        // If the file doesn't exist, no big deal, we will create it
        // if (err) {
        //   return console.log(err);
        // }

        var the_same = true;
        for(index in output[command]){
          if(!file_content || !output[command] || !file_content[index] || output[command][index] != file_content[index]) {
            the_same = false;
            console.log("updating file " + command_file_prefix + command + command_file_postfix);
          write_and_emit (socket, command, output);
          break;
          }
        }
        if(the_same) {
          return true;
        }
      });
    }
    else {
      console.log("creating file: " + command_file_prefix + command.replace(remove_non_aplhanumeric_regex, replace_non_alphanumeric_with) + command_file_postfix)
      write_and_emit (socket, command, output);
    }
  })
}

function write_and_emit (socket, filename, output) {
  fs.writeFile(command_file_prefix + filename.replace(remove_non_aplhanumeric_regex, replace_non_alphanumeric_with) + command_file_postfix, output[filename], function(err) {
    if(err) {
      return console.log(err);
    }
  });

  emit(socket, "info", output);
}


function run_command (socket, processor, data, command) {

  var command_hash = {}, start_time, end_time;

  command_hash[command] = {};

  command_hash['command'] = command;

  start_time = Date.now();

  command_hash['output'] = shell_command ("bash", command);

  end_time = Date.now();

  command_hash['runtime'] = end_time - start_time;

  command_hash['token'] = data.processor.token;

  var message_label = "command";

  emit (socket, message(processor, message_label, command_hash));
}


if(local_config && local_config.data_processors){

  servers.forEach(function(server, index){

    var processor = {

          server: server,
          token: token,
        }

    // command_file_postfix = "-" + server.match(/\d+\.\d+\.\d+\.\d+(:\d+)?/)[0];

    sockets[server] = require('socket.io-client')(server,
    {
      path: '/socket.io-client'
    });

    var config, previous_config, interval;

    function onConnect(socket) {

      socket.on('configuration', function (data) {

        recieve(socket, data);

        config = YAML.parse(data.content);

        clearInterval(interval);

        previous_config = YAML.parse(data.content);

        function emit_interval(data, config) {

          // if (config && config.commands) {
          //
          //   config.commands.forEach(function(command) {
          //
          //     run_command (socket, processor, data, command);
          //   });
          // };

          if (config && config.tenets) {

            config.tenets.forEach(function(tenent) {

              tenent.sections.forEach(function(section, index) {

                // spread commands out uding a delay
                setTimeout(function (){

                  run_command (socket, processor, data, section.command);
                }, 500 * index);

              });
            });
          };
        };

        interval = setInterval(function () {

          console.log("keepalive");

          var message_label = "keepalive";

          var message_content = data.processor.token;

          emit (socket, message(processor, message_label, message_content));

          emit_interval (data, config);

        }, config.interval);

        emit_interval(data, config);

        socket.on('disconnect', function(message) {

          clearInterval(interval);

          console.log("disconnected: " + processor.server);
        });

        socket.on('reset', function (data) {

          console.log("host updated: " + processor.server);

          clearInterval(interval);

          get_config(processor, socket);
        });

      });

      socket.on('not found', function (data) {

        recieve(socket, data);

        process.exit();
      });
    };

    onConnect(sockets[processor.server]);

    sockets[processor.server].on('connect', function (socket) {
      // socket connected
      console.log("connected: " + processor.server);

      get_config(processor, sockets[processor.server]);
    });
  })
}
