# System Monitor Client
NodeJS real-time system monitor client

# client.js
- Reads command configuration file from `config` directory based on operating 
system
- Establishes websocket connection with monitor server
- Recieves server configuration in websocket message to determine the 
information and the system command output to send back to the server
- Runs system commands & sends output back to the server as a websocket message